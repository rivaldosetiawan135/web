# Lima

Lima is an open source graphics driver which supports Mali-400/450 embedded
GPUs from ARM via reverse engineering.
It was upstreamed in mesa 19.1 and linux kernel 5.2.

This page aims to provide an overview and track the status of the project.
Note that as lima is now part of the respective upstream projects, it is
increasingly less necessary to take special steps to install or use it; the goal
is that it becomes just another driver automatically available in mesa and
linux distribution builds.

Note that the Mali GPU is only for rendering, not for controlling a display.
Each SoC has its own separate display engine to control the display output.
Therefore, to display the contents rendered by the Mali GPU, a separate display
driver is also required.
In mesa, this is handled by the `kmsro` infrastructure. If you want a display,
make sure `kmsro` is also enabled in your mesa build and that your kernel also
includes both lima and the driver for your display engine.

Lima is a project still in heavy development. If possible, please try
the latest mesa master branch before reporting issues, as they may
have already been worked on. In the current state, there is
little-to-none support/backport to older releases.

## Status
### Applications

Note: The following is not a exhaustive list of applications or test targets.
Please report an issue if other applications misbehave.

* desktop
  * Wayland: weston, sway, etc.
  * Xorg+modesetting+glamor (has known issues)
* Games
  * supertuxkart (GL2 renderer)
  * Quake 3 arena
* User Applications
  * Firefox (webgl)
* Android
  * Users reported early working status of lima on android. Please follow:
    * https://gitlab.freedesktop.org/lima/mesa/issues/120
* off-screen rendering
  * off-screen reference application at https://github.com/yuq/gfx/tree/master/gbm-surface
* Test applications
  * kmscube
  * glxgears, es2\_gears
  * glmark2-es2, glmark2-es2-drm, glmark2-es2-wayland
  * https://github.com/yuq/gfx
* Test/development suites
  * piglit
  * deqp
  * shader-db

### Tested on
* Mali-400
* Mali-450

### Display drivers
* Allwinner: `sun4i-drm`
* Amlogic: `meson`
* Exynos: `exynos`
* Ericsson MCDE: `mcde`
* Rockchip: `rockchip`
* Tinydrm: `tinydrm`

## Issues
Please try the latest mesa master branch or at least mesa latest release before
reporting issues.
Please review the mesa3d bug report [guidelines](https://www.mesa3d.org/bugs.html).

File and track issues in the [upstream mesa issue tracker](https://gitlab.freedesktop.org/mesa/mesa/issues?label_name%5B%5D=lima).
lima tags will be added accordingly by the developers.

[apitrace](https://github.com/apitrace/apitrace) traces are very welcome in
issue reports and significantly ease the debug and fix process.

## Development
The lima driver has been upstreamed to linux kernel and mesa, so work is done on
the upstream repos now. The mesa and linux repos in the freedesktop lima gitlab
group are deprecated.

### kernel driver
kernel driver changes need to be sent to the [drm-misc repo](https://cgit.freedesktop.org/drm/drm-misc/).
There is no gitlab project for it, so we have to send patches to dri-devel
mailing list for review, please also CC lima mailing list to get lima
developers' attention.

Reference: https://dri.freedesktop.org/docs/drm/gpu/introduction.html

### mesa driver
mesa driver changes need to be sent to [mesa repo](https://gitlab.freedesktop.org/mesa/mesa).
We prefer to send gitlab MR for review which ease the review process and has CI
test. Please add `lima` MR tag to get lima developers' attention.

After review and CI pass, please rebase the MR with reviewers'
Reviewed-by/Acked-by/Tested-by tags added and select the checkbox of: `Allow
commits from members who can merge to the target branch` to permit other
developers to merge your MR if you don't have merge permission.

Reference: https://www.mesa3d.org/submittingpatches.html

### doc
For project topics, still this readme page. MRs can be submitted to this page.
For technical topics, we may write into upstream repos, like kernel
Documentation/gpu/lima.rst.

## Build and install

Lima is now included in upstream mesa, so there are no lima-specific steps other
than just enabling lima in mesa.
Lima is now increasingly being included in popular linux distributions so it
might be just be included in your kernel and mesa-dri-drivers or equivalent
package.
In this case, nothing needs to be done to install lima on your system.
Below is a quick start guide, but keep in mind that these are just standard mesa
build steps as described in the [mesa documentation](https://www.mesa3d.org/meson.html).

### common process
1. build Linux kernel with lima driver from: https://cgit.freedesktop.org/drm/drm-misc/
2. build mesa with lima driver from: https://gitlab.freedesktop.org/mesa/mesa

mesa build
```
meson build -Dvulkan-drivers=[] -Dplatforms=x11,drm,wayland -Ddri-drivers=[] -Dgallium-drivers=lima,kmsro
ninja -C build install
```

### xorg.conf
For xserver >=1.20, if you have issues with starting X, you may try the
following xorg.conf:
```
Section "ServerFlags"
        Option  "AutoAddGPU" "off"
        Option "Debug" "dmabuf_capable"
EndSection

Section "OutputClass"
        Identifier "Lima"
        MatchDriver "<display DRM driver>"
        Driver "modesetting"
        Option "PrimaryGPU" "true"
EndSection
```
Note that the Mali GPU is only for rendering, each SoC has its own separate
display engine.
You need to find your display engine DRM driver name and replace `<display DRM driver>`
with it. For example, see the list of display drivers mentioned above.

## Dump tool
* A tool to dump the runtime of the closed source mali driver for reverse
engineering is available at:
  * https://gitlab.freedesktop.org/lima/mali-syscall-tracker

## TODO
### mesa
* List of known [mesa issues](https://gitlab.freedesktop.org/mesa/mesa/issues?label_name%5B%5D=lima).
* PP compiler optimization, add post scheduler instruction combination which
combine multi instructions into one instruction
* optimization
  * thread rendering that has dedicated render thread to submit command

### kernel
* Power management

## Communication channels

* \#lima channel on IRC freenode
* lima [mailing list](https://lists.freedesktop.org/mailman/listinfo/lima)
* dri-devel [mailing list](https://lists.freedesktop.org/mailman/listinfo/dri-devel)

## Reference
* Luc Verhaegen's original Lima site: http://web.archive.org/web/20180106112822/http://limadriver.org/
* compiler: https://gitorious.org/open-gpu-tools/cwabbotts-open-gpu-tools/
